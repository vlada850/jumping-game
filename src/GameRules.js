import React from "react";
import jumpingGameMovementRulePic from './pics/jumping-game-movement-rule.png'

const GameRules = () => (
    <div className='game-rules'>
    <p>
        Player has a board of 10*10 boxes. When a player clicks on an empty box in the board, a level is generated and boxes that the player can click on are shown. The number of boxes the player can click is defined by the current level. To complete a level, the player must click on all generated fields in a specific order. The player cannot click random fields, he can only click fields that pass a specific rule set by the game.
        Each new level will have one more box than the previous level.
        To complete the whole game, the player must finish all 99 levels.
    </p>
    <h3>Lives</h3>
    <p>
        Whenever the player completes a level he gains a life.
        Whenever the player fails to complete a level, he loses a number of lives equal to the number of unclicked boxes in that level.
        The player can then choose from which level he will start a new game as long as he has completed that level.
        When he reaches zero lives, he starts from level 1 again.
    </p>
    <h3>Movement rules</h3>
    <p>
        You can move not more or less than three fields vertically and horizontally, or you can move not more or less than two fields diagonally.
    </p>
    <img src={jumpingGameMovementRulePic} alt="game movement rule"/>
    </div>
)

export default GameRules