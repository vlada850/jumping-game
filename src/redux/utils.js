const getRow = (rowIndex, rowLength) => {
  const row = [];
  for (let i = 1; i <= rowLength; i++) {
    row.push(rowIndex * rowLength + i);
  }
  return row;
};

const getBoard = (rowLength, columnLength) => {
  const board = [];
  for (let i = 0; i < columnLength; i++) {
    board.push(getRow(i, rowLength));
  }
  return board;
};

const board = getBoard(10, 10);

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

const getCandidateCells = ({ clickedRow, clickedColumn }) => {
  const candidateCells = [];
  const colLength = board.length;
  const rowLength = board[0].length;

  candidateCells.push({ row: clickedRow, col: clickedColumn - 3 });
  candidateCells.push({ row: clickedRow, col: clickedColumn + 3 });
  candidateCells.push({ row: clickedRow - 3, col: clickedColumn });
  candidateCells.push({ row: clickedRow + 3, col: clickedColumn });
  candidateCells.push({ row: clickedRow - 2, col: clickedColumn - 2 });
  candidateCells.push({ row: clickedRow - 2, col: clickedColumn + 2 });
  candidateCells.push({ row: clickedRow + 2, col: clickedColumn - 2 });
  candidateCells.push({ row: clickedRow + 2, col: clickedColumn + 2 });

  return candidateCells.filter(
    cell => !(cell.row < 0 || cell.row >= rowLength || cell.col < 0 || cell.col >= colLength),
  );
};

const getAllowedCell = id => {
  let clickedRow = 0;
  let clickedColumn = 0;
  if (id.toString().length === 1) {
    clickedRow = 0;
    clickedColumn = id - 1;
  } else if (id.toString().length === 3) {
    clickedRow = 9;
    clickedColumn = 9;
  } else {
    if (id.toString().charAt(1) === '0') {
      clickedRow = Number(id.toString().charAt(0)) - 1;
      clickedColumn = Number(id.toString().charAt(1)) + 9;
    } else {
      clickedRow = Number(id.toString().charAt(0));
      clickedColumn = Number(id.toString().charAt(1)) - 1;
    }
  }

  const candidateCells = getCandidateCells({ clickedRow, clickedColumn });
  const len = candidateCells.length;

  const clickableCell = candidateCells[getRandomInt(len)];

  return Number(String(clickableCell.row).concat(String(clickableCell.col))) + 1;
};

export const calculateClickableCells = (id, level) => {
  let newId = id;
  let newArr = [];
  if (level < 66) {
    for (let i = 1; newArr.length <= level; i++) {
      for (let j = 0; j < level; j++) {
        newArr.includes(newId)
          ? getAllowedCell(newArr.pop(j)) && (newId = newArr.pop(j))
          : newArr.push(newId) && (newId = getAllowedCell(newId));
      }
    }
    newArr.shift();
    return newArr.slice(0, level);
  }
  if (level >= 66) {
    for (let i = 1; newArr.length < 66; i++) {
      for (let j = 0; j < level; j++) {
        newArr.includes(newId)
          ? getAllowedCell(newArr.pop(j)) && (newId = newArr.pop(j))
          : newArr.push(newId) && (newId = getAllowedCell(newId));
      }
    }
    for (let k = 1; k < level - 66; k++) {
      for (let l = 1; l <= 100; l++) {
        newArr.includes(l) ? console.log('Bad Idea') : newArr.push(l);
      }
    }
    newArr.shift();
    return newArr.slice(0, level);
  }
  return newArr;
};

export const recommendedClickableCells = id => {
  const newArr = [];
  let clickedRow = 0;
  let clickedColumn = 0;
  if (id.toString().length === 1) {
    clickedRow = 0;
    clickedColumn = id - 1;
  } else if (id.toString().length === 3) {
    clickedRow = 9;
    clickedColumn = 9;
  } else {
    if (id.toString().charAt(1) === '0') {
      clickedRow = Number(id.toString().charAt(0)) - 1;
      clickedColumn = Number(id.toString().charAt(1)) + 9;
    } else {
      clickedRow = Number(id.toString().charAt(0));
      clickedColumn = Number(id.toString().charAt(1)) - 1;
    }
  }

  const candidateCells = getCandidateCells({ clickedRow, clickedColumn });

  for (let i = 0; i < candidateCells.length; i++) {
    newArr.push(Number(String(candidateCells[i].row).concat(String(candidateCells[i].col))) + 1);
  }
  return newArr;
};

export const hint = (recommendedCells, id) => {
  const arr = [];
  const recommendedCellsFiltered = recommendedCells.map(c => id - c);
  recommendedCellsFiltered.map(c => {
    switch (c) {
      case 30:
        return arr.push('3 steps up, ');
      case -30:
        return arr.push('3 steps down, ');
      case -3:
        return arr.push('3 steps forward, ');
      case 3:
        return arr.push('3 steps back, ');
      case 18:
        return arr.push('2 steps up right diagonally, ');
      case -18:
        return arr.push('2 steps down left diagonally, ');
      case 22:
        return arr.push('2 steps up left diagonally, ');
      case -22:
        return arr.push('2 steps down right diagonally, ');
      default:
        return '';
    }
  });
  return arr;
};
