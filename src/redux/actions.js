export const setInitialBoard = cells => {
  return {
    type: 'SET_INITIAL_BOARD',
    cells: cells,
  };
};

export const extractCell = (className, id) => {
  return {
    type: 'EXTRACT_CELL',
    className: className,
    id: id,
  };
};

export const setLevel = level => {
  return {
    type: 'LEVEL_PICKER',
    level: level,
  };
};
