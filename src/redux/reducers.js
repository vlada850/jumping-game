import { combineReducers } from 'redux';

let initLvlAndLives = 1;

const cells = (state = [], action) => {
  switch (action.type) {
    case 'SET_INITIAL_BOARD':
      return action.cells;
    case 'CELL_CLICKED':
      return [
        ...state.map(c =>
          c.props.id === action.cell.id ? { ...c, props: { className: 'cellOnInitialClick', id: c.props.id } } : c,
        ),
      ];
    case 'CLICKABLE_CELLS':
      return [
        ...state.map(c =>
          action.arr.includes(c.props.id) ? { ...c, props: { className: 'clickableCell', id: c.props.id } } : c,
        ),
      ];
    case 'RECOMMENDED_CELLS':
      return [
        ...state.map(c =>
          action.recommendedCells.includes(c.props.id) && c.props.className === 'clickableCell'
            ? { ...c, props: { className: 'recommendedCell', id: c.props.id } }
            : action.recommendedCells.includes(c.props.id) === false && c.props.className === 'recommendedCell'
            ? { ...c, props: { className: 'clickableCell', id: c.props.id } }
            : c,
        ),
      ];
    case 'LEVEL_WON':
    case 'LEVEL_LOST':
      return [...state.map(c => (c ? { ...c, props: { className: 'cell', id: c.props.id } } : ''))];
    default:
      return state;
  }
};

const clicked = (state = 0, action) => {
  switch (action.type) {
    case 'EXTRACT_CELL':
      return state + 1;
    case 'LEVEL_WON':
    case 'LEVEL_LOST':
      return 0;
    default:
      return state;
  }
};

const leftToClick = (state = 0, action) => {
  switch (action.type) {
    case 'CELL_CLICKED':
      return action.level - action.clicked + 1;
    default:
      return state;
  }
};

const level = (state = initLvlAndLives, action) => {
  switch (action.type) {
    case 'LEVEL_PICKER':
      initLvlAndLives = action.level;
      return action.level;
    case 'LEVEL_WON':
      return state + 1;
    case 'LEVEL_LOST':
      return action.lives - (state - action.clicked + 2) > 1 ? state : initLvlAndLives;
    default:
      return state;
  }
};

const lives = (state = initLvlAndLives, action) => {
  switch (action.type) {
    case 'LEVEL_PICKER':
      initLvlAndLives = action.level;
      return action.level;
    case 'LEVEL_WON':
      return state + 1;
    case 'LEVEL_LOST':
      return state - (action.level - action.clicked + 2) > 1
        ? state - (action.level - action.clicked + 2)
        : initLvlAndLives;
    default:
      return state;
  }
};

const cell = (state = {}, action) => {
  switch (action.type) {
    case 'EXTRACT_CELL':
      return { className: action.className, id: action.id };
    default:
      return state;
  }
};

const recommendedCells = (state = [], action) => {
  switch (action.type) {
    case 'RECOMMENDED_CELLS':
      return action.recommendedCells;
    case 'LEVEL_WON':
    case 'LEVEL_LOST':
      return [];
    default:
      return state;
  }
};

const hint = (state = [], action) => {
  switch (action.type) {
    case 'RECOMMENDED_CELLS':
      return action.hint;
    default:
      return state;
  }
};

const clickableCells = (state = [200], action) => {
  switch (action.type) {
    case 'CLICKABLE_CELLS':
      return action.arr;
    case 'CELL_CLICKED':
      return state.includes(action.cell.id) && action.cell.className === 'recommendedCell'
        ? state.filter(c => c !== action.cell.id)
        : action.clicked > 1
        ? [404]
        : state;
    case 'LEVEL_WON':
    case 'LEVEL_LOST':
      return [200];
    default:
      return state;
  }
};

const actionTrigger = (state = false, action) => {
  switch (action.type) {
    case 'EXTRACT_CELL':
      return !state;
    case 'CELL_CLICKED':
      return !state;
    default:
      return state;
  }
};

const actionTriggerForRecommendedCells = (state = false, action) => {
  switch (action.type) {
    case 'CELL_CLICKED':
      return !state;
    case 'RECOMMENDED_CELLS':
      return !state;
    default:
      return state;
  }
};

const actionTriggerForClickableCells = (state = false, action) => {
  switch (action.type) {
    case 'CELL_CLICKED':
      return action.clicked === 1;
    case 'CLICKABLE_CELLS':
      return false;
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  cells,
  cell,
  actionTrigger,
  actionTriggerForClickableCells,
  actionTriggerForRecommendedCells,
  recommendedCells,
  clickableCells,
  level,
  lives,
  clicked,
  leftToClick,
  hint,
});

export default rootReducer;
