const preloadedState = {
  cells: [],
  cell: {},
  actionTrigger: false,
  actionTriggerForClickableCells: false,
  actionTriggerForRecommendedCells: false,
  recommendedCells: [],
  clickableCells: [200],
  level: 1,
  lives: 1,
  clicked: 0,
  hint: [],
};

export default preloadedState;
