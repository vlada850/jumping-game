const onEveryCellClicked = store => next => action => {
  let result = next(action);

  let { clicked, cell, actionTrigger, level } = store.getState();

  if (actionTrigger) {
    store.dispatch({
      type: 'CELL_CLICKED',
      cell: cell,
      clicked: clicked,
      level: level,
    });
  }

  return result;
};

export default onEveryCellClicked;
