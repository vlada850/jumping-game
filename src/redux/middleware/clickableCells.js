import { calculateClickableCells } from '../utils';

const ClickableCells = store => next => action => {
  let result = next(action);

  let { cell, level, actionTriggerForClickableCells } = store.getState();

  const clickableCellsFunc = async () => {
    const arr = await calculateClickableCells(cell.id, level);
    store.dispatch({ type: 'CLICKABLE_CELLS', arr: arr, id: cell.id });
  };

  if (actionTriggerForClickableCells) {
    clickableCellsFunc();
  }

  return result;
};

export default ClickableCells;
