const LevelWonAnLost = store => next => action => {
  let result = next(action);

  let { clickableCells, clicked, level, lives } = store.getState();

  if (clickableCells[0] === 404) {
    store.dispatch({
      type: 'LEVEL_LOST',
      clicked: clicked,
      level: level,
      lives: lives,
    });
  }

  if (clickableCells.length === 0) {
    store.dispatch({
      type: 'LEVEL_WON',
    });
  }

  return result;
};

export default LevelWonAnLost;
