import { hint } from '../utils';
import { recommendedClickableCells } from '../utils';

const RecommendedCells = store => next => action => {
  let result = next(action);

  let { cell, clicked, actionTriggerForRecommendedCells, clickableCells } = store.getState();

  const recommendedCells = async () => {
    const arr = await recommendedClickableCells(cell.id);
    const filteredArr = clickableCells.filter(c => arr.includes(c));
    const arrHint = await hint(filteredArr, cell.id);
    store.dispatch({
      type: 'RECOMMENDED_CELLS',
      recommendedCells: arr,
      id: cell.id,
      clicked: clicked,
      hint: arrHint,
    });
  };

  if (actionTriggerForRecommendedCells) {
    recommendedCells();
  }

  return result;
};

export default RecommendedCells;
