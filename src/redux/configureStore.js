import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import monitorReducersEnhancer from './enhancers/monitorReducers';
import loggerMiddleware from './middleware/logger';
import OnEveryCellClicked from './middleware/onEveryCellClicked';
import ClickableCells from './middleware/clickableCells';
import RecommendedCells from './middleware/recommendedCells';
import LevelWonAnLost from './middleware/levelWonAndLost';

import rootReducer from './reducers';

export default function configureStore(persistedState) {
  const middlewares = [loggerMiddleware, OnEveryCellClicked, RecommendedCells, LevelWonAnLost, ClickableCells];
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const enhancers = [middlewareEnhancer, monitorReducersEnhancer];
  const composedEnhancers = composeWithDevTools(...enhancers);

  const store = createStore(rootReducer, persistedState, composedEnhancers);

  return store;
}
