// generating cells for any board size

const getRow = (rowIndex, rowLength) => {
  const row = [];
  for (let i = 1; i <= rowLength; i++) {
    row.push(rowIndex * rowLength + i);
  }
  return row;
};

// console.log(getRow(2, 10));

const getBoard = (rowLength, columnLength) => {
  const board = [];
  for (let i = 0; i < columnLength; i++) {
    board.push(getRow(i, rowLength));
  }
  return board;
};

const board = getBoard(10, 10);

// console.log(board);

const getCandidateCells = ({ clickedRow, clickedColumn, board }) => {
  const candidateCells = [];
  const colLength = board.length;
  const rowLength = board[0].length;

  candidateCells.push({ row: clickedRow, col: clickedColumn - 3 });
  candidateCells.push({ row: clickedRow, col: clickedColumn + 3 });
  candidateCells.push({ row: clickedRow - 3, col: clickedColumn });
  candidateCells.push({ row: clickedRow + 3, col: clickedColumn });
  candidateCells.push({ row: clickedRow - 2, col: clickedColumn - 2 });
  candidateCells.push({ row: clickedRow - 2, col: clickedColumn + 2 });
  candidateCells.push({ row: clickedRow + 2, col: clickedColumn - 2 });
  candidateCells.push({ row: clickedRow + 2, col: clickedColumn + 2 });

  return candidateCells.filter(
    cell => !(cell.row < 0 || cell.row >= rowLength || cell.col < 0 || cell.col >= colLength),
  );
};

// console.log(getCandidateCells({ clickedRow: 5, clickedColumn: 0, board }));

const getAllowedCell = ({ clickedRow, clickedColumn, board }) => {
  const candidateCells = getCandidateCells({ clickedRow, clickedColumn, board });
  const len = candidateCells.length;

  return candidateCells[getRandomInt(len)];
};

const getClickableCells = ({ clickedRow, clickedColumn, board, level }) => {
  const visitedCells = [{ row: clickedRow, col: clickedColumn }];
  const clickableCells = [];

  let allowedCellMaybe = getAllowedCell({ clickedRow, clickedColumn, board });
  while (clickableCells.length < level) {
    while (visitedCells.some(vc => vc.row === allowedCellMaybe.row && vc.col === allowedCellMaybe.col)) {
      allowedCellMaybe = getAllowedCell({ clickedRow, clickedColumn, board });
    }
    clickableCells.push(allowedCellMaybe);
    visitedCells.push(allowedCellMaybe);

    allowedCellMaybe = getAllowedCell({ clickedRow: allowedCellMaybe.row, clickedColumn: allowedCellMaybe.col, board });
  }

  return clickableCells;
};
console.log(getAllowedCell({ clickedRow: 5, clickedColumn: 0, board}));
console.log(getClickableCells({ clickedRow: 5, clickedColumn: 0, board, level: 3 }));

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}


/*const generateAllowedArr = id => {
  switch (id) {
    case 1:
    case 2:
    case 11:
    case 12:
      return [3, 22, 30];
    case 3:
    case 13:
      return [3, 18, 22, 30];
    case 4:
    case 5:
    case 6:
    case 7:
    case 14:
    case 15:
    case 16:
    case 17:
      return [-3, 3, 18, 22, 30];
    case 8:
    case 18:
      return [-3, 18, 22, 30];
    case 9:
    case 10:
    case 19:
    case 20:
      return [-3, 18, 30];
    case 21:
    case 22:
      return [-18, 3, 22, 30];
    case 23:
      return [-18, -22, 3, 18, 22, 30];
    case 24:
    case 25:
    case 26:
    case 27:
      return [-3, -18, -22, 3, 18, 22, 30];
    case 28:
      return [-3, -18, -22, 18, 22, 30];
    case 29:
    case 30:
      return [-3, -22, 18, 30];
    case 31:
    case 32:
    case 41:
    case 42:
    case 51:
    case 52:
    case 61:
    case 62:
      return [-18, -30, 3, 22, 30];
    case 33:
    case 43:
    case 53:
    case 63:
      return [-18, -22, -30, 3, 18, 22, 30];
    case 34:
    case 35:
    case 36:
    case 37:
    case 44:
    case 45:
    case 46:
    case 47:
    case 54:
    case 55:
    case 56:
    case 57:
    case 64:
    case 65:
    case 66:
    case 67:
      return [-3, -18, -22, -30, 3, 18, 22, 30];
    case 38:
    case 48:
    case 58:
    case 68:
      return [-3, -18, -22, -30, 18, 22, 30];
    case 39:
    case 40:
    case 49:
    case 50:
    case 59:
    case 60:
    case 69:
    case 70:
      return [-3, -22, -30, 18, 30];
    case 71:
    case 72:
      return [-18, -30, 3, 22];
    case 73:
      return [-18, -22, -30, 3, 18, 22];
    case 74:
    case 75:
    case 76:
    case 77:
      return [-3, -18, -22, -30, 3, 18, 22];
    case 78:
      return [-3, -18, -22, -30, 18, 22];
    case 79:
    case 80:
      return [-3, -22, -30, 18];
    case 81:
    case 82:
    case 91:
    case 92:
      return [-18, -30, 3];
    case 83:
    case 93:
      return [-18, -22, -30, 3];
    case 84:
    case 85:
    case 86:
    case 87:
    case 94:
    case 95:
    case 96:
    case 97:
      return [-3, -18, -22, -30, 3];
    case 88:
    case 98:
      return [-3, -18, -22, -30];
    case 89:
    case 90:
    case 99:
    case 100:
      return [-3, -22, -30];
    default:
      return id;
  }
};*/

/*const generateRandomAllowedNumber = id => {
  const arr = generateAllowedArr(id);
  return id + arr[[Math.floor(Math.random() * arr.length)]];
};*/
