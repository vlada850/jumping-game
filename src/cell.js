import React from 'react';
import { extractCell } from './redux/actions';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

const Cell = ({ className, id }) => {
  const dispatch = useDispatch();

  return <div className={className} onClick={() => dispatch(extractCell(className, id))}></div>;
};

Cell.propTypes = {
  className: PropTypes.string.isRequired,
};

export default Cell;
