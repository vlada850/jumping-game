import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import configureStore from './redux/configureStore';
/*import preloadedState from './redux/preloadedState';*/
import { loadState, saveState } from './localStorage';

const persistedState = loadState();

const store = configureStore(persistedState);

store.subscribe(() => {
  saveState({
    level: store.getState().level,
    lives: store.getState().lives,
  });
});

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
