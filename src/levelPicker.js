import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Slider from '@material-ui/core/Slider';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

import { setLevel } from './redux/actions';
import {connect} from "react-redux";

const useStyles = makeStyles(theme => ({
  root: {
    width: 300 + theme.spacing(3) * 2,
    padding: theme.spacing(2),
  },
  margin: {
    height: theme.spacing(3),
  },
}));

function ValueLabelComponent(props) {
  const { children, open, value } = props;

  const popperRef = React.useRef(null);
  React.useEffect(() => {
    if (popperRef.current) {
      popperRef.current.update();
    }
  });

  return (
    <Tooltip
      PopperProps={{
        popperRef,
      }}
      open={open}
      enterTouchDelay={0}
      placement="top"
      title={value}
    >
      {children}
    </Tooltip>
  );
}

ValueLabelComponent.propTypes = {
  children: PropTypes.element.isRequired,
  open: PropTypes.bool.isRequired,
  value: PropTypes.number.isRequired,
};

const PrettoSlider = withStyles({
  root: {
    color: '#52af77',
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus,&:hover,&$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);

function CustomizedSlider(props) {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Typography gutterBottom>Choose your level</Typography>
      <div className={classes.margin} />
      <div className={classes.margin} />
      <PrettoSlider
        valueLabelDisplay="auto"
        aria-label="pretto slider"
        min={1}
        max={99}
        onClick={sliderProps => props.dispatch(setLevel(Number(sliderProps.target.textContent)))}
        defaultValue={10}
      />
      <div className={classes.margin} />
    </Paper>
  );
}

CustomizedSlider.propTypes = {
  target: PropTypes.object,
  textContent: PropTypes.string,
};

export default connect()(CustomizedSlider);
