import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import { setInitialBoard } from './redux/actions';
import Cell from './cell';
import TransitionsModal from './modalLvlPicker';
import GameRules from "./GameRules";
import Button from '@material-ui/core/Button';
const uuidv4 = require('uuid/v4');

class App extends Component {
  componentDidMount() {
    const cellsArr = [];
    for (let i = 1; i < 101; i++) {
      cellsArr.push(<Cell id={i} key={uuidv4()} className="cell" />);
    }
    this.props.dispatch(setInitialBoard(cellsArr));
  }

  state = {
    open: false,
    openGameRules: false
  };

  handleState = () => this.setState({ open: !this.state.open });

  handleOpenGameRules = () => this.setState({ openGameRules: !this.state.openGameRules });

  render() {
    const { cells, clicked, level, lives, leftToClick, hint } = this.props;
    const { openGameRules } = this.state;
    return (
      <div className='wrapper'>
        <div className="board">
          <div className="boardTable">{cells}</div>
          <div className="gameStats">
            <div className="stats">
              <h1>Game Stats</h1>
              <p>
                Clicked: <span className="clickedSpan">{clicked}</span>
              </p>
              <p>
                Level: <span>{level}</span>
              </p>
              <p>
                Lives: <span>{lives}</span>
              </p>
              <p>
                Left to click: <span>{leftToClick}</span>
              </p>
              <TransitionsModal />
              <Button color="primary" onClick={this.handleState}>
                <span>Hint</span>
              </Button>
            </div>
            {this.state.open ? (
              <div className="tablet">
                <p>
                  Allowed movement:
                  <br />
                  <br />
                  {hint}
                </p>
              </div>
            ) : null}
          </div>
        </div>
        {openGameRules && <GameRules />}
        <div className='header'>
          <button className={openGameRules ? 'game-rules-button-open' : 'game-rules-button'} onClick={this.handleOpenGameRules}>Game Rules</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    cells: state.cells,
    cell: state.cell,
    clicked: state.clicked,
    level: state.level,
    lives: state.lives,
    leftToClick: state.leftToClick,
    hint: state.hint,
  };
};

App.propTypes = {
  cells: PropTypes.array.isRequired,
  cell: PropTypes.object.isRequired,
  clicked: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
  lives: PropTypes.number.isRequired,
  dispatch: PropTypes.func.isRequired,
  leftToClick: PropTypes.number.isRequired,
  hint: PropTypes.array.isRequired,
};

export default connect(mapStateToProps)(App);
